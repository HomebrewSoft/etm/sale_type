# -*- coding: utf-8 -*-
from odoo import models, fields, api, _


class SaleOrderType(models.Model):
    _name = 'sale.order.type'
    _description = 'Sale Order Type'

    name = fields.Char(
        required=True,
    )
