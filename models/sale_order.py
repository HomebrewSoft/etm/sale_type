# -*- coding: utf-8 -*-
from odoo import models, fields, api, _


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    type_id = fields.Many2one(
        comodel_name='sale.order.type',
    )

    @api.model
    def create(self, vals):
        sale_order = super(SaleOrder, self).create(vals)
        if vals.get('type_id') == self.env.ref('sale_type.sale_order_type_refilled_with_changes').id:
            sale_order_model = self.env['ir.model'].search([('model', '=', self._name)], limit=1)
            self.env['mail.activity'].create({
                'date_deadline': fields.Date.context_today(self),
                'res_id': sale_order.id,
                'res_model_id': sale_order_model.id,
                'user_id': self.env.user.id,
            })
        return sale_order
