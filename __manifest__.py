# -*- coding: utf-8 -*-
{
    'name': 'Sale Type',
    'version': '13.0.1.0.0',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        'sale',
    ],
    'data': [
        'data/sale_order_type.xml',
        'security/ir.model.access.csv',
        'views/sale_order_type.xml',
        'views/sale_order.xml',
    ],
}
